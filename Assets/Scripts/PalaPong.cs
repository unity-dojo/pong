using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalaPong : MonoBehaviour
{
    public Transform pelota;

    public float velocidad=1;
    public float tiempoRespuestaIA = 1f;

    public float limiteSuperior;
    public float limiteInferior;

    public bool jugador1 = true;
    public bool jugadorHumano = true;

    void Update()
    {

        float velocidadAplicada = velocidad * Time.deltaTime;

        bool j1Subir = Input.GetKey(KeyCode.W);
        bool j1Bajar = Input.GetKey(KeyCode.S);

        bool j2Subir = Input.GetKey(KeyCode.UpArrow);
        bool j2Bajar = Input.GetKey(KeyCode.DownArrow);

        if (jugador1 == true)
        {
            Movimiento(j1Subir, j1Bajar, velocidadAplicada);
        }
        else
        {
            // JUGADOR 2
            if (jugadorHumano==true)
            {
                Movimiento(j2Subir, j2Bajar, velocidadAplicada);
            }
            else
            {
                Vector3 posicion = new Vector3(transform.position.x,pelota.position.y, transform.position.z);

                posicion.y = Mathf.Lerp(transform.position.y, posicion.y, tiempoRespuestaIA);
                
                transform.position = posicion;
            }
        }
    }


    void Movimiento(bool jSubir, bool jBajar, float velocidadAplicada)
    {
        if (jSubir == true)
        {
            if (transform.position.y < limiteSuperior)
            {
                transform.Translate(0, velocidadAplicada, 0);
            }
        }

        if (jBajar == true)
        {
            if (transform.position.y > limiteInferior)
            {
                transform.Translate(0, -velocidadAplicada, 0);
            }
        }
    }

    

    public void Humanificar(bool humano)
    {
        jugadorHumano = humano;
    }
}
